$(function () {
    var table = $('#cims_data_table').DataTable({
        processing: true,
        serverSide: true,

        // paging: true,
        // pageLength: 100, 
        
        ajax: base_url + "/admin/query-optimize/data-table",
        columns: [
        // {
        //     data: 'id',
        //     name: 'id'
        // },
        {
            data: 'name',
            name: 'name'
        },
        {
            data: 'email',
            name: 'email'
        },
        // {
        //     data: 'status',
        //     name: 'status',
        //     orderable: false,
        //     searchable: false
        // },
        // {
        //     data: 'action',
        //     name: 'action',
        //     orderable: false,
        //     searchable: false
        // },
        ]
    });

    function reload_table() {
        table.DataTable().ajax.reload(null, false);
    }
})