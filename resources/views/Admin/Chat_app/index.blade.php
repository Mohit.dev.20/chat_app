@section('meta_title') Dashboard | Construction Inventory @endsection
@extends('Admin.Layouts.layout')

@section('css')
<style>
    .user-image {
        width: 60px;
        height: 60px;
        object-fit: cover;
        border-radius: 50%;
    }

    .chat-box{
        overflow-y: auto;
    }

    .text-design{
        float: left;
        margin: 0px;
        padding: 10px;
        border: 1px solid deepskyblue;
        background-color: #dcf5fd;
        border-radius: 20px;
        max-width: 75%;
    }

    .active{
        background-color: deepskyblue;
        color: white;
        border-radius: 8px;
    }
  </style>
@endsection

@section('content')
<div class="content-page">
    <div class="container">
        <div class="row" style="height: 80vh;">
            <div class="col-md-4" style="height: 80vh;">
                <div class="card mb-2">
                    <div class="card-body p-0" id="select-chat-type">
                        <div class="icons d-flex justify-content-center" style="font-size: x-large; cursor: pointer;">
                            <div class="col-md-6 d-flex justify-content-center py-2 px-0" id="open_chat"><i class="fa fa-user"></i></div>
                            <div class="col-md-6 d-flex justify-content-center py-2 px-0 active" id="open_group_chat"><i class="fa fa-users"></i></div>
                        </div>
                    </div>
                </div>
                <div class="card mb-0">
                    <div class="card-body" id="search-box">
                        <div class="search-container d-flex justify-content-around">
                            <div class="col-md-12">
                                <input type="text" class="form-control" placeholder="Search...">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="chat-list">
                    @foreach($users as $user)
                    <div class="card mb-0">
                        <div class="card-body py-0">
                            <div class="row user-card" data-id="{{ $user->id }}" data-name="{{ $user->user_name }}" style="cursor: pointer">
                                <div class="col-md-3">
                                    <img src="{{ URL::asset('package_assets/images/default-images/default-user.jpg') }}" class="card-img-top user-image" alt="User Image">
                                </div>
                                <div class="col-md-8 d-flex align-items-center">
                                    <h5>{{ !empty($user->user_name) ? $user->user_name : '' }} <sup><span id="{{$user->id}}-status" class="my-0 text-success d-none" style="font-size: 50px;">.</span></sup> </h5>
                                </div>
                                <div class="col-md-1">
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
            <div class="col-md-8">
                <div class="card d-none" id="chat-section" style="height: 82vh; border-color: deepskyblue; border-width: 2px;">
                    <div class="card-header" style="background-color: deepskyblue;">
                        <h4 id="chat-user" class="text-light">User Name</h4>
                    </div>
                    <div class="card-body chat-box" id="chat-box">
                    </div>
                    <div class="card-footer">
                        <form id="chat-form">
                            <div class="input-group">
                                <input type="text" class="form-control" id="message-input" placeholder="Type your message...">
                                <div class="input-group-append">
                                    <button type="submit" class="btn text-light" id="send-message-btn" style="background-color: deepskyblue;">Send</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="card" id="select-user-to-chat" style="height: 80vh;">
                    <div class="d-flex justify-content-center align-items-center" style="height: 80vh;">
                        <h2>Select User To Chat</h2>
                    </div>
                </div>
            </div>
        </div>

        <!-- Modal -->
        <div class="modal fade" id="CreateGroupModal" tabindex="-1" role="dialog" aria-labelledby="CreateGroupModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="CreateGroupModalLabel">Create Group</h5>
                    <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="{{ route('create_group') }}" method="post" id="create-group-form">
                    <div class="modal-body">
                        @csrf
                        <label for="group_name" class="form-label">Group Name</label>
                        <input type="text" class="form-control mb-2" id="group_name" name="group_name" required="required">
                        <label for="group_image_path" class="form-label">Group Name</label>
                        <input type="file" class="form-control mb-2" id="group_image_path" name="group_image_path"  required="required">
                        <label for="group_limit" class="form-label">Group Name</label>
                        <input type="text" class="form-control mb-2" id="group_limit" name="group_limit" required="required">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary"> Submit</button>
                    </div>
                </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection 

@section('script')
<script>
    var sender_id = @json(Auth::guard('master_admins')->user()->id);
    var receiver_id;
</script>

<script>
    {!! Vite::content('resources/js/app.js') !!}
</script>

<script type="text/javascript">
    window.Echo.channel('public-message').listen('PublicMessageEvent', (e) => {
        console.log(e);
    });

    Echo.private('chatroom').listen('ChatApp', (event) => {
        console.log(event);
    });

    Echo.join('status-update')
    .here((users) => {
        for(let x=0; x < users.length; x++){
            if(sender_id != users[x]['id']){
                $("#"+users[x]['id']+"-status").removeClass("d-none");
            }
        }
    })
    .joining((user) => {
        $("#"+user.id+"-status").removeClass("d-none");
        console.log(user.user_name+" joined");
    })
    .leaving((user) => {
        $("#"+user.id+"-status").addClass("d-none");
        console.log(user.user_name+" leaved");
    })
    .listen('UserStatusEvent', (event) => {
        // console.log(event);
    })

    Echo.private('broadcast-message').listen('.getChatMessage', (data) => {
        if(sender_id == data.chat.receiver_id && receiver_id == data.chat.sender_id){
            console.log(data);
            var html = `
                <div class="current-user-message">
                    <div class="row">
                        <div class="col-12">
                            <p class="text-design my-1" style="float: left;">`+data.chat.message+`</p>
                        </div>
                    </div>
                </div>
            `;
            $("#chat-box").append(html);
            scroll_chat();
        }
    })

</script>

<script>
    $(document).ready(function(){
        $(document).on("click", ".user-card", function(){
            $("#select-user-to-chat").addClass('d-none')
            $("#chat-section").removeClass('d-none')
            $("#chat-box").html('')
            $("#chat-user").html($(this).attr("data-name"));
            receiver_id = $(this).attr("data-id");
            load_message();
        })
        send_message();
        open_group_chat();
        open_chat();
    })

    function load_message(){
        $.ajax({
            type: 'post',
            url: '{{ route("load_message") }}',
            headers: {
                "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
            },
            data: {
                sender_id : sender_id,
                receiver_id: receiver_id,
            },
            success: function(response){
                if(response.success){
                    let chats = response.data;
                    let html = '';
                    for(var i = 0; i < chats.length; i++){
                        if(chats[i].sender_id == sender_id){
                            var float = 'right'
                        } else {
                            var float = 'left'
                        }
                        html += `
                        <div class="current-user-message">
                            <div class="row">
                                <div class="col-12">
                                    <p class="text-design my-1" style="float: `+float+`">`+chats[i].message+`</p>
                                </div>
                            </div>
                        </div>
                        `;
                    }
                    $("#chat-box").append(html);
                    scroll_chat();
                } else {
                    console.log(response.message);
                }
            }
        })
    }

    function send_message(){
        $("#chat-form").submit(function(event){
            event.preventDefault();
            $.ajax({
                type: 'post',
                url: '{{ route("send_message") }}',
                headers: {
                    "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
                },
                data: {
                    sender_id : sender_id,
                    receiver_id: receiver_id,
                    message: $("#message-input").val()
                },
                success: function(response){
                    if(response.status){
                        var html = `
                        <div class="current-user-message">
                            <div class="row">
                                <div class="col-12">
                                    <p class="text-design my-1" style="float: right;">`+response.data.message+`</p>
                                </div>
                            </div>
                        </div>
                        `;
                    }
                    $("#chat-box").append(html);
                    $("#message-input").val('');
                    scroll_chat();
                }
            })
        })
    }

    function scroll_chat(){
        $("#chat-box").animate({
            scrollTop: $("#chat-box").offset().top + $("#chat-box")[0].scrollHeight
        }, 500)
    }

    function open_group_chat(){
        $(document).on("click", "#open_group_chat", function(){
            $("#open_chat").removeClass("active");
            let html = `
                <div class="search-container d-flex justify-content-between">
                    <div class="col-md-7">
                        <input type="text" class="form-control" placeholder="Search...">
                    </div>
                    <div class="col-md-1"></div>
                    <div class="col-md-4">
                        <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#CreateGroupModal" style="float: right;">Add Group</button>
                    </div>
                </div>
            `
            $("#search-box").html(html);
            $(this).addClass("active");
            get_all_groups();
        })
        create_group();
    }

    function get_all_groups(){
        $.ajax({
                type: "post",
                url: "{{ route('get_all_group') }}",
                headers: {
                    "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
                },
                success: function(response){
                    if(response.status){
                        let html = "";
                        for(let i = 0; i < response.data.length; i++){
                            html += `
                                <div class="card mb-0">
                                    <div class="card-body py-0">
                                        <div class="row user-card" group-id="`+response.data[i].id+`" group-name="`+response.data[i].group_name+`" style="cursor: pointer">
                                            <div class="col-md-3">
                                                <img src="{{ URL::asset('package_assets/images/default-images/default-users.jpg') }}" class="card-img-top user-image" alt="User Image">
                                            </div>
                                            <div class="col-md-8 d-flex align-items-center">
                                                <h5> `+response.data[i].group_name+` </h5>
                                            </div>
                                            <div class="col-md-1">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            `;
                        }
                        $("#chat-list").html(html);

                       console.log(html);
                    } else {
                        console.log(response);
                    }
                }
            })
    }

    function create_group(){
        $("#create-group-form").submit(function(event){
            event.preventDefault();
            $.ajax({
                type: "post",
                url: "{{ route('create_group') }}",
                headers: {
                    "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
                },
                data: new FormData(this),
                contentType: false,
                processData: false,
                success: function(response){
                    if(response.status){
                        $("#CreateGroupModal").modal('hide');
                        success_toast("success", "Group Created Successfully");
                    } else {
                        console.log(response);
                    }
                }
            })
        });
    }

    function open_chat(){
        $(document).on("click", "#open_chat", function(){
            $("#open_group_chat").removeClass("active");
            let html = `
                <div class="search-container d-flex justify-content-between">
                    <div class="col-md-12">
                        <input type="text" class="form-control" placeholder="Search...">
                    </div>
                </div>
            `
            $("#search-box").html(html);
            $(this).addClass("active");
        })
    }
</script>



@endsection
