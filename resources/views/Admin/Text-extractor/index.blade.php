@section('meta_title') Text Extractor | My Works @endsection
@extends('Admin.Layouts.layout')

@section('css')
<style>
</style>
@endsection

@section('content')
<div class="content-page">
    <div class="container">
        <div class="row">
            <div class="col-md-2">
            </div>
            <div class="col-md-4">
                <div class="card p-4 m-4">
                    <div class="input-group mb-2" style="justify-content: center;">
                        <form action="{{ route('get_text') }}" method="post" enctype="multipart/form-data" id="text-image-form">
                            @csrf
                            <input type="file" class="dropify" data-plugins="dropify" data-show-remove="false" id="text_image_path" name="text_image_path" data-default-file="{{ !empty($organisation->organisation_logo_image_path) && Storage::exists($organisation->organisation_logo_image_path) ? url('/').Storage::url($organisation->organisation_logo_image_path) : '' }}" />
                            <button type="submit" class="btn btn-submit btn-primary mt-2" id="submit_btn"> {{ !empty($organisation->id) ? 'Update' : 'Submit' }} </button>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card p-4 m-4">
                    <div class="input-group mb-2" style="justify-content: center;">
                        <h4>{{ !empty($text) ? $text : ''}}</h4>
                    </div>
                </div>
            </div>
            <div class="col-md-2">
            </div>
        </div>

    </div>
</div>
@endsection 

@section('script')

@endsection
