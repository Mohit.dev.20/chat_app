<?php

namespace App\Traits;

use Aws\Rekognition\RekognitionClient;

trait TextExtractorTrait
{
    protected $rekognition;

    public function __construct()
    {
        $this->rekognition = new RekognitionClient([
            'region' => env('AWS_DEFAULT_REGION'),
            'version' => 'latest',
            'credentials' => [
                'key' => env('AWS_ACCESS_KEY_ID'),
                'secret' => env('AWS_SECRET_ACCESS_KEY'),
            ],
        ]);
    }

    public function detectTextInImage($imagePath)
    {
        $image = fopen($imagePath, 'r');
        $bytes = fread($image, filesize($imagePath));
        fclose($image);

        $result = $this->rekognition->detectText([
            'Image' => [
                'Bytes' => $bytes,
            ],
        ]);

        return $result->get('TextDetections');
    }
}
