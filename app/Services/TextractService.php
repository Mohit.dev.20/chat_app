<?php

namespace App\Services;

use Aws\Textract\TextractClient;

class TextractService
{
    protected $textract;

    public function __construct()
    {
        $this->textract = new TextractClient([
            'region' => env('AWS_DEFAULT_REGION'),
            'version' => 'latest',
            'credentials' => [
                'key' => env('AWS_ACCESS_KEY_ID'),
                'secret' => env('AWS_SECRET_ACCESS_KEY'),
            ],
        ]);
    }

    public function detectTextInDocument($documentPath)
    {
        $file = fopen($documentPath, 'r');
        $bytes = fread($file, filesize($documentPath));
        fclose($file);

        $result = $this->textract->analyzeDocument([
            'Document' => [
                'Bytes' => $bytes,
            ],
            'FeatureTypes' => ['TABLES', 'FORMS']
        ]);

        return $result;
    }
}
