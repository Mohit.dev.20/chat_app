<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class SendMessageEvent implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    private $chatData;
    public function __construct($chatData)
    {
        $this->chatData = $chatData;
    }

    public function broadcastWith(): array
    {
        return ['chat' => $this->chatData];
    }

    public function broadcastAs()
    {
        return 'getChatMessage';
    }

    public function broadcastOn(): array
    {
        return [
            new PrivateChannel('broadcast-message'),
        ];
    }
}
