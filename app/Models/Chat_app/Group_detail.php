<?php

namespace App\Models\Chat_app;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Group_detail extends Model
{
    use HasFactory;
    protected $fillable = [
        'creater_id',
        'group_name',
        'group_limit',
        'group_image_path',
        'group_image_name',
       
        'created_ip_address',
        'modified_ip_address',
        'created_by',
        'modified_by',
        'status',
    ];
}
