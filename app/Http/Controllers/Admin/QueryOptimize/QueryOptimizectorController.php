<?php

namespace App\Http\Controllers\Admin\QueryOptimize;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use Yajra\DataTables\DataTables;
use Yajra\DataTables\QueryDataTable;
use DB;

class QueryOptimizectorController extends Controller
{
    public function index(){
        // DB::table('users')->orderBy('id')->limit(10000)->chunk(1000, function ($users) {
        //     foreach ($users as $user) {
        //         //
        //     }
        // });
        
        return view('Admin.Query-optimize.index');
    }

    public function data_table(Request $request){
        $users = User::orderBy('id','DESC')->limit(1000)->select('id', 'name', 'email')->paginate(1000);
        
        if ($request->ajax()) {
            // $data = collect();
    
            // User::chunk(1000, function ($items) use ($data) {
            //     foreach ($items as $item) {
            //         $data->push($item);
            //     }
            // });
    
            return DataTables::of($users)
                ->addIndexColumn()
                ->addColumn('name', function ($row) {
                    return !empty($row->name) ? $row->name : '';
                })
                ->addColumn('email', function ($row) {
                    return !empty($row->email) ? $row->email : '';
                })
                ->make(true);
        }
    }
}
