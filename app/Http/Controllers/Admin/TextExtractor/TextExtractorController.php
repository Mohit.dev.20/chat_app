<?php

namespace App\Http\Controllers\Admin\TextExtractor;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Services\RekognitionService;
use App\Services\TextractService;
use App\Traits\MediaTrait;

class TextExtractorController extends Controller
{
    use MediaTrait;
    protected $rekognitionService;
    protected $textractService;

    public function __construct(RekognitionService $rekognitionService, TextractService $textractService)
    {
        $this->rekognitionService = $rekognitionService;
        $this->textractService = $textractService;
    }

    public function index(){
        return view('Admin.Text-extractor.index');
    }

    public function get_text_from_image(Request $request){
        $path = $request->file('text_image_path')->getPathName();
        $textDetections = $this->rekognitionService->detectTextInImage($path);
        dd($textDetections);
        $extractedText = '';
        foreach ($textDetections as $text) {
            if($text['Type'] == 'LINE'){
                $extractedText .= $text['DetectedText'] . ' ';
            }
        }
        dd($extractedText);
        return response()->json([
            'extracted_text' => trim($extractedText)
        ]);
    }

    public function get_text_from_document(Request $request){
        $path = $request->file('text_image_path')->getPathName();
        $result = $this->textractService->detectTextInDocument($path);
        $textDetections = $result->get('Blocks');

        dd($textDetections);

        $documentText = '';
        foreach($textDetections as $detection){
            // if($detection['BlockType'] === 'LINE'){
                $documentText .= $detection['Text'].' ';
            // }
        }

        dd($documentText);

        return response()->json([
            'extracted_text' => trim($textDetections)
        ]);
    }
}
