<?php

namespace App\Http\Controllers\Admin\Chat_app;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Master\Master_admin;
use App\Events\ChatApp;
use App\Events\SendMessageEvent;
use Illuminate\Support\Facades\Auth;
use App\Models\Chat_app\Message;

class ChatAppController extends Controller
{
    public function index(){
        $users = Master_admin::where('status', 'active')->where('id', '!=', Auth::guard('master_admins')->user()->id)->orderBy('id', 'desc')->get();
        return view('Admin.Chat_app.index', compact('users'));
    }

    public function sendMessage(Request $request){
        $input['sender_id'] = $request->sender_id;
        $input['receiver_id'] = $request->receiver_id;
        $input['message'] = $request->message;
        $input['created_ip_address'] = $request->ip();
        $input['created_by'] = Auth::guard('master_admins')->user()->id;
        $message = Message::create($input);

        $broadcast = broadcast(new SendMessageEvent($message));

        return response()->json(['status' => true, 'data' => $message]);
    }

    public function loadMessage(Request $request){
        $messages = Message::where(function($query) use ($request){
            $query->where('sender_id', $request->sender_id)->orWhere('sender_id', $request->receiver_id);
        })->where(function($query) use ($request){
            $query->where('receiver_id', $request->sender_id)->orWhere('receiver_id', $request->receiver_id);
        })->get();

        return response()->json(['success' => true, 'data' => $messages]);
    }
}
