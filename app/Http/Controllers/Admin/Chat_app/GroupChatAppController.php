<?php

namespace App\Http\Controllers\Admin\Chat_app;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Chat_app\Group_detail;
use App\Traits\MediaTrait;

class GroupChatAppController extends Controller
{
    use MediaTrait;

    public function index(){
        $groups = Group_detail::where('status', 'active')->get();
        return response()->json(['status' => true, 'data' => $groups]);
    }

    public function create_group(Request $request){
        $input['creater_id'] = Auth::guard('master_admins')->user()->id;
        $input['group_name'] = $request->group_name;
        $input['group_limit'] = $request->group_limit;
        if($request->has('file')){
            $this->uploadFiles($request->group_image_path, '/images');
        }
        $group_detail = Group_detail::create($input);
        return response()->json(['status' => true, 'data' => $group_detail]);
    }


}
