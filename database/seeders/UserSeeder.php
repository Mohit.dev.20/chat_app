<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\User;
use App\Models\User_new;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        // Seed 100,000 users
        // User::factory()->count(1000000)->create();

        // Number of users to seed
        $totalRecords = 10100000; // 1 crore (10 million)
        $chunkSize = 10000; // Number of records to insert per batch

        // Insert in chunks
        for ($i = 0; $i < $totalRecords; $i += $chunkSize) {
            User_new::factory()->count($chunkSize)->create();
            echo "Inserted {$i} records.\n"; // Progress output
        }
    }
}

