<?php

use Illuminate\Support\Facades\Broadcast;
use Illuminate\Broadcasting\PrivateChannel;
use App\Models\Master\Master_admin;

/*
|--------------------------------------------------------------------------
| Broadcast Channels
|--------------------------------------------------------------------------
|
| Here you may register all of the event broadcasting channels that your
| application supports. The given channel authorization callbacks are
| used to check if an authenticated user can listen to the channel.
|
*/

// Broadcast::channel('App.Models.Master_admin.{id}', function ($user, $id) {
//     return (int) $user->id === (int) $id;
// });

Broadcast::channel('chatroom', function ($user) {
    return true;
}, ['guards' =>'master_admins']);

Broadcast::channel('status-update', function ($user) {
    return $user;
}, ['guards' =>'master_admins']);

Broadcast::channel('broadcast-message', function ($user) {
    return $user;
}, ['guards' =>'master_admins']);
